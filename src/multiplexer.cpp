// multiplexer
// sel = "00" selects data1
// sel = "01" selects adder_out
// sel = "1-" selects nand_out

#include "multiplexer.hpp"

using namespace std;


void Multiplexer::do_selection()
{
   	sc_bv<16> temp;
   	sc_bv<2> sel_signal = sel.read();

   	if( sel_signal == "00" )
   	{
      		temp = in1.read();
   	}
   	else if ( sel_signal == "01" )
   	{
      		temp = in2.read();
   	}
   	else
   	{
      		temp = in3.read();
   	}

   	result.write(temp);
}
