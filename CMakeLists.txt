cmake_minimum_required (VERSION 2.6)

include_directories ("include")

add_library (multiplexer_stimulus "src/multiplexer_stimulus.cpp")

add_library (multiplexer "src/multiplexer.cpp")

add_executable (multiplexer_tb test/multiplexer_tb.cpp)

target_link_libraries (multiplexer_tb multiplexer_stimulus multiplexer systemc)

enable_testing ()

add_test (Completes multiplexer_tb)
