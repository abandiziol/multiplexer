#include <systemc.h>

SC_MODULE(Multiplexer_Stimulus){

	//Declaration of the inputs
	sc_in<bool> clock;
	sc_in<sc_bv<16> > result;

	//Declaration of the outputs
	sc_out<sc_bv<16> > in1;
   	sc_out<sc_bv<16> > in2;
   	sc_out<sc_bv<16> > in3;
   	sc_out<sc_bv<2> > sel;


	void stimgen();

	SC_CTOR(Multiplexer_Stimulus){

		SC_THREAD(stimgen);
		sensitive_pos << clock;	

	}

};
