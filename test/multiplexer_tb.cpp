#include "systemc.h"
#include <iostream>
#include "multiplexer_stimulus.hpp"
#include "multiplexer.hpp"

int sc_main (int argc, char* argv[]) {
	sc_signal<sc_bv<16> > in1;
   	sc_signal<sc_bv<16> > in2;
   	sc_signal<sc_bv<16> > in3;
   	sc_signal<sc_bv<2> > sel;
   	sc_signal<sc_bv<16> > result;
	sc_clock clock("clock");

	Multiplexer_Stimulus s1("Multiplexer_Stimulus");
	
	s1.in1(in1);
	s1.in2(in2);
	s1.in3(in3);
	s1.sel(sel);
	s1.result(result);
	s1.clock(clock);

	Multiplexer m1("Multiplexer");
	
	m1.in1(in1);
	m1.in2(in2);
	m1.in3(in3);
	m1.sel(sel);
	m1.result(result);
	m1.clock(clock);

	//Open VCD file
	sc_trace_file *wf = sc_create_vcd_trace_file("register_file");

	//Dump the desired signals
	sc_trace(wf, clock, "clock");
	sc_trace(wf, in1, "in1");
	sc_trace(wf, in2, "in2");
	sc_trace(wf, in3, "in3");
	sc_trace(wf, sel, "sel");
	sc_trace(wf, result, "result");

	sc_start(SC_ZERO_TIME);
	sc_start(100, SC_NS);

	sc_close_vcd_trace_file(wf);
	return 0;

}
