# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/andreabandiziol/Systemc-trials/multiplexer

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/andreabandiziol/Systemc-trials/multiplexer/build

# Include any dependencies generated for this target.
include CMakeFiles/multiplexer.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/multiplexer.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/multiplexer.dir/flags.make

CMakeFiles/multiplexer.dir/src/multiplexer.cpp.o: CMakeFiles/multiplexer.dir/flags.make
CMakeFiles/multiplexer.dir/src/multiplexer.cpp.o: ../src/multiplexer.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/andreabandiziol/Systemc-trials/multiplexer/build/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/multiplexer.dir/src/multiplexer.cpp.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/multiplexer.dir/src/multiplexer.cpp.o -c /home/andreabandiziol/Systemc-trials/multiplexer/src/multiplexer.cpp

CMakeFiles/multiplexer.dir/src/multiplexer.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/multiplexer.dir/src/multiplexer.cpp.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/andreabandiziol/Systemc-trials/multiplexer/src/multiplexer.cpp > CMakeFiles/multiplexer.dir/src/multiplexer.cpp.i

CMakeFiles/multiplexer.dir/src/multiplexer.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/multiplexer.dir/src/multiplexer.cpp.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/andreabandiziol/Systemc-trials/multiplexer/src/multiplexer.cpp -o CMakeFiles/multiplexer.dir/src/multiplexer.cpp.s

CMakeFiles/multiplexer.dir/src/multiplexer.cpp.o.requires:
.PHONY : CMakeFiles/multiplexer.dir/src/multiplexer.cpp.o.requires

CMakeFiles/multiplexer.dir/src/multiplexer.cpp.o.provides: CMakeFiles/multiplexer.dir/src/multiplexer.cpp.o.requires
	$(MAKE) -f CMakeFiles/multiplexer.dir/build.make CMakeFiles/multiplexer.dir/src/multiplexer.cpp.o.provides.build
.PHONY : CMakeFiles/multiplexer.dir/src/multiplexer.cpp.o.provides

CMakeFiles/multiplexer.dir/src/multiplexer.cpp.o.provides.build: CMakeFiles/multiplexer.dir/src/multiplexer.cpp.o

# Object files for target multiplexer
multiplexer_OBJECTS = \
"CMakeFiles/multiplexer.dir/src/multiplexer.cpp.o"

# External object files for target multiplexer
multiplexer_EXTERNAL_OBJECTS =

libmultiplexer.a: CMakeFiles/multiplexer.dir/src/multiplexer.cpp.o
libmultiplexer.a: CMakeFiles/multiplexer.dir/build.make
libmultiplexer.a: CMakeFiles/multiplexer.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX static library libmultiplexer.a"
	$(CMAKE_COMMAND) -P CMakeFiles/multiplexer.dir/cmake_clean_target.cmake
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/multiplexer.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/multiplexer.dir/build: libmultiplexer.a
.PHONY : CMakeFiles/multiplexer.dir/build

CMakeFiles/multiplexer.dir/requires: CMakeFiles/multiplexer.dir/src/multiplexer.cpp.o.requires
.PHONY : CMakeFiles/multiplexer.dir/requires

CMakeFiles/multiplexer.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/multiplexer.dir/cmake_clean.cmake
.PHONY : CMakeFiles/multiplexer.dir/clean

CMakeFiles/multiplexer.dir/depend:
	cd /home/andreabandiziol/Systemc-trials/multiplexer/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/andreabandiziol/Systemc-trials/multiplexer /home/andreabandiziol/Systemc-trials/multiplexer /home/andreabandiziol/Systemc-trials/multiplexer/build /home/andreabandiziol/Systemc-trials/multiplexer/build /home/andreabandiziol/Systemc-trials/multiplexer/build/CMakeFiles/multiplexer.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/multiplexer.dir/depend

